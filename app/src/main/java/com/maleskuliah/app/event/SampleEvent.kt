package com.maleskuliah.app.event

/**
 * Created by MuhammadLucky on 8/18/2018.
 * For eventbus event
 */
class SampleEvent(param: Any?) {
    var param: Any? = null

    init {
        this.param = param
    }
}