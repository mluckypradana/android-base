package com.maleskuliah.app.application

import android.annotation.SuppressLint
import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import android.support.v7.app.AppCompatDelegate
import com.crashlytics.android.Crashlytics
import com.maleskuliah.app.R
import com.maleskuliah.app.api.Api
import com.orhanobut.hawk.Hawk
import io.fabric.sdk.android.Fabric


/**
 * Created by MuhammadLucky on 14/05/2018.
 */
class App : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        context = this
        api = Api.getAPIInterface()

        //Init notification for oreo
        initNotificationChannel()

        //Init hawk preference
        Hawk.init(context).build()
        Fabric.with(this, Crashlytics())
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    private fun initNotificationChannel() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val notificationChannel = NotificationChannel(
                    getString(R.string.default_notification_channel_id),
                    getString(R.string.notification_channel_default_name),
                    NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }

    companion object {

        /**
         * The application [Context] made static.
         * Do **NOT** use this as the context for a view,
         * this is mostly used to simplify calling of resources
         * (esp. String resources) outside activities.
         */
        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context
        lateinit var api: Api.APIInterface
            private set

        // This flag should be set to true to enable VectorDrawable support for API < 21
        init {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        }

        //Activity list to finish
        var activities: MutableList<Activity> = arrayListOf()
    }
}