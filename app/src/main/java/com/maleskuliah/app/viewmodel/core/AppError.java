package com.maleskuliah.app.viewmodel.core;

import android.support.annotation.Nullable;

/**
 * Created by Alvin on 10/10/16.
 * <p/>
 * The custom error codes are:
 * * 1000~1999 - Client error
 * * 2000~2999 - Development error
 */
public class AppError extends Exception {

    public static final int CLIENT_UNKNOWN = 1000;
    public static final int CLIENT_CANCELLED = 1001;
    public static final int CLIENT_CONNECTION = 1002;
    public static final int CLIENT_TIMEOUT = 1003;
    public static final int DEVELOPMENT_UNKNOWN = 2000;
    public static final int DEVELOPMENT_NULL_POINTER = 2001;
    public static final int DEVELOPMENT_CLASS_CAST = 2002;

    /** The error code */
    private int mCode;

    /** The error message */
    @Nullable
    private String mError;

    /** Default constructor */
    public AppError(int code) {
        mCode = code;
    }

    /** Default constructor */
    public AppError(int code, @Nullable String detailMessage) {
        super(detailMessage);
        mCode = code;
        mError = detailMessage;
    }

    public int getCode() {
        return mCode;
    }

    public void setCode(int code) {
        mCode = code;
    }

    @Nullable
    public String getError() {
        return mError;
    }

    public void setError(@Nullable String error) {
        mError = error;
    }
}
