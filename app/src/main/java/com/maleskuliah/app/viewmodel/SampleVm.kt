package com.maleskuliah.app.viewmodel

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import com.kopnus.kdigi.presenter.callback.FetchDataCallback
import com.maleskuliah.app.model.local.Sample
import com.maleskuliah.app.model.response.core.CoreResp
import com.maleskuliah.app.viewmodel.core.CoreVm
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by MuhammadLucky on 14/05/2018.
 */
typealias CORE = Sample

typealias LIST = CoreResp<MutableList<CORE>>
typealias DATA = CoreResp<CORE>

class SampleVm : CoreVm {
    constructor(activity: FragmentActivity) : super(activity)
    constructor(fragment: Fragment) : super(fragment)
    constructor() : super()

    fun fetchSamples(callback: FetchDataCallback?) {
        setCallbackForVm(callback)

        //Api
        //liveData.value = Resource.loading()
        api.fetchSamples().enqueue(object : Callback<LIST> {
            override fun onFailure(call: Call<LIST>?, t: Throwable?) {
                handleError(t, callback)
            }

            override fun onResponse(call: Call<LIST>?,
                                    response: Response<LIST>?) {
                if (isInvalid(response, callback)) return
                val resp = response?.body()?.data
                onSuccess(callback, resp)
            }
        })
    }

    fun fetchSample(id: String, callback: FetchDataCallback?) {
        setCallbackForVm(callback)

        //Api
        api.fetchSample(id).enqueue(object : Callback<DATA> {
            override fun onFailure(call: Call<DATA>?, t: Throwable?) {
                handleError(t, callback)
            }

            override fun onResponse(call: Call<DATA>?,
                                    response: Response<DATA>?) {
                if (isInvalid(response, callback)) return
                val resp = response?.body()?.data
                onSuccess(callback, resp)
            }
        })
    }


}