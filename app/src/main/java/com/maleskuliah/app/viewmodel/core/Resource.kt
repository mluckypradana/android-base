package com.maleskuliah.app.viewmodel.core

import android.support.annotation.IntDef
import com.maleskuliah.app.R
import com.maleskuliah.app.application.App

/**
 * Created by Alvin Rusli on 06/07/2017.
 * TODO Set error String to AppError type
 *
 * The resource object for view models.
 */
class Resource
/** Private constructor  */
private constructor(
        /** The current data status  */
        @param:Status @field:Status
        /** @return the status
         */
        /**
         * Sets this resource's status.
         * @param status the status
         */
        var status: Int,
        /** The actual data  */
        /** @return the data
         */
        /**
         * Sets this resource's data.
         * @param data the data
         */
        var data: Any?, error: String?) {

    /** The error  */
    /** @return the error
     */
    /**
     * Sets this resource's error.
     * @param error the error
     */
    var error: String

    @IntDef(LOADING, SUCCESS, ERROR)
    annotation class Status

    init {
        if (error == null)
            this.error = App.context.getString(R.string.error_generic)
        else
            this.error = error
    }

    companion object {

        const val LOADING = 0
        const val SUCCESS = 1
        const val ERROR = 2

        /** Creates a new loading resource object  */
        fun loading(): Resource {
            return Resource(LOADING, null, null)
        }

        /**
         * Creates a new loading resource object.
         * @param data the data to be set
         */
        fun loading(data: Any?): Resource {
            return Resource(LOADING, data, null)
        }

        /**
         * Creates a new successful resource object.
         * @param data the data to be set
         */
        fun success(data: Any?): Resource {
            return Resource(SUCCESS, data, null)
        }

        /**
         * Creates a new error resource object.
         * @param error the error
         */
        fun error(error: String?): Resource {
            return Resource(ERROR, null, error)
        }

        /**
         * Creates a new error resource object.
         * @param error the error
         */
        fun error(error: String, data: Any?): Resource {
            return Resource(ERROR, data, error)
        }

    }
}
