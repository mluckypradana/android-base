package com.maleskuliah.app.viewmodel

import com.maleskuliah.app.config.Config.CURRENT_PREF_VERSION
import com.maleskuliah.app.config.Prefs
import com.maleskuliah.app.viewmodel.core.CoreVm
import com.orhanobut.hawk.Hawk


/**
 * Created by MuhammadLucky on 14/05/2018.
 */

object PrefVm : CoreVm() {

    /** Clear all pref related if version or class attributes is upgraded */
    fun checkVersion() {
        val version = Hawk.get<Int?>(Prefs.PREF_VERSION)
        if (version != CURRENT_PREF_VERSION) {
            deleteAllPrefs()
            Hawk.put(Prefs.PREF_VERSION, CURRENT_PREF_VERSION)
        }
    }

    /** Delete all prefs from hawk */
    private fun deleteAllPrefs() {
        Hawk.deleteAll()
    }

    /** Save temporary data, pass activities */
    fun setData(data: Any) {
        Hawk.put(Prefs.TEMP_DATA, data)
    }

    fun getData(): Any? {
        return Hawk.get(Prefs.TEMP_DATA)
    }

}