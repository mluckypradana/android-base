package com.maleskuliah.app.viewmodel.core

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import com.google.gson.Gson
import com.maleskuliah.app.BuildConfig
import com.maleskuliah.app.R
import com.maleskuliah.app.api.Api
import com.maleskuliah.app.api.callback.CoreCallback
import com.maleskuliah.app.application.App
import com.maleskuliah.app.model.response.core.ErrorResp
import retrofit2.Response
import java.nio.charset.Charset

/**
 * https://developer.android.com/topic/libraries/architecture/viewmodel
 * This viewmodel implemented only for API related
 */
open class CoreVm : ViewModel {
    var api: Api.APIInterface = App.api
    @SuppressLint("StaticFieldLeak")
    var activity: FragmentActivity? = null
    var fragment: Fragment? = null
    private var vm: CoreVm? = null
    private val liveData = MutableLiveData<Resource>()
    private var withLiveData = false

    constructor()

    constructor(activity: FragmentActivity) {
        vm = ViewModelProviders.of(activity).get(javaClass)
    }

    constructor(fragment: Fragment) {
        vm = ViewModelProviders.of(fragment).get(javaClass)
    }

    /** Set callback for view model>live data, call this before request */
    internal fun setCallbackForVm(callback: CoreCallback?) {
        withLiveData = true
        val observer = Observer<Resource> { resource ->
            if (resource != null)
                when (resource.status) {
                    Resource.ERROR -> callback?.onFailure(resource.error)
                    Resource.SUCCESS -> callback?.onSuccess(resource.data)
                }
        }

        when {
            activity != null -> liveData.observe(activity!!, observer)
            fragment != null -> liveData.observe(fragment!!, observer)
            else -> withLiveData = false
        }
    }

    /** If response is invalid return false & callback.onFailure */
    internal fun isInvalid(response: Response<*>?, callback: CoreCallback?): Boolean {
        //If response is valid
        if (response!!.isSuccessful && response.body() != null)
            return false

        //If response code is invalid
        else {
            var errorResponse: ErrorResp? = null
            //Get error body
            try {
                val bytes = response.errorBody()!!.bytes()
                errorResponse = Gson().fromJson(String(bytes, Charset.forName("UTF-8")), ErrorResp::class.java)
            } catch (ignored: Exception) {
            }

            //Need auto logout
            if (response.code() == 401 || response.code() == 666) {
                onFailure(callback, App.context.getString(R.string.message_session_ended))
//                UserPresenter().logout()
//                App.context.startActivity(Intent(App.context, MainAct::class.java))
            }

            //Another common error
            else
                onFailure(callback,
                        when {
                            errorResponse != null -> errorResponse.message
                            BuildConfig.FLAVOR == "production" -> App.context.getString(R.string.error_generic)
                            response.code() == 400 -> App.context.getString(R.string.error_conn_400)
                            response.code() == 401 -> App.context.getString(R.string.error_conn_401)
                            response.code() == 404 -> App.context.getString(R.string.error_conn_404)
                            response.code() == 408 -> App.context.getString(R.string.error_conn_408)
                            response.code() == 501 -> App.context.getString(R.string.error_conn_501)
                            response.code() == 502 -> App.context.getString(R.string.error_conn_502)
                            response.code() == 503 -> App.context.getString(R.string.error_conn_503)
                            response.code() == 504 -> App.context.getString(R.string.error_conn_504)
                            else -> App.context.getString(R.string.error_generic)
                        }
                )
            return true
        }
    }

    /** If api call failed, call callback.onFailure */
    internal fun handleError(t: Throwable?, callback: CoreCallback?) {
        val message =
                if (!BuildConfig.DEBUG) App.context.getString(R.string.error_generic)
                else t?.localizedMessage
        onFailure(callback, message)
    }

    /** Send callback with or without live data, host for vm must not be null */
    internal fun onSuccess(callback: CoreCallback?, data: Any?) {
        if (withLiveData && (activity != null || fragment != null))
            liveData.value = Resource.success(data)
        else
            callback?.onSuccess(data)
    }

    /** Send callback with or without live data, host for vm must not be null */
    private fun onFailure(callback: CoreCallback?, message: String?) {
        if (withLiveData && (activity != null || fragment != null))
            liveData.value = Resource.error(message)
        else
            callback?.onFailure(message)
    }

}
