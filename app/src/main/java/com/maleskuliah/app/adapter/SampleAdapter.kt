package com.maleskuliah.app.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.maleskuliah.app.R
import com.maleskuliah.app.adapter.core.CoreAdapter
import com.maleskuliah.app.listener.OnItemClickListener
import com.maleskuliah.app.model.local.Sample
import kotlinx.android.synthetic.main.item_sample.view.*

/**
 * Created by MuhammadLucky on 24/11/2017.
 */
typealias T_CORE = Sample

class SampleAdapter(list: MutableList<T_CORE>, listener: OnItemClickListener) : CoreAdapter<T_CORE>() {
    val layout = R.layout.item_sample

    init {
        this.list = list
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(parent.context).inflate(layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind()
    }

    internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            val data = list[adapterPosition]
            itemView.setOnClickListener { listener?.onItemClicked(adapterPosition) }
            itemView.tv_title.text = data.title
        }

    }
}