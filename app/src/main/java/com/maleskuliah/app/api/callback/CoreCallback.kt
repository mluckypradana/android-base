package com.maleskuliah.app.api.callback

interface CoreCallback {
    /** On general failure */
    fun onFailure(message: String?)

    /** On general success */
    fun onSuccess(response: Any? = null)
}
