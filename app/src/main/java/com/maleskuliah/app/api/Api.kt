package com.maleskuliah.app.api

import com.maleskuliah.app.R
import com.maleskuliah.app.application.App
import com.maleskuliah.app.model.local.Sample
import com.maleskuliah.app.model.response.core.CoreResp
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by Alvin Rusli on 7/28/2016.
 *
 * The API interface service.
 */
object Api {

    /** The [APIInterface] object */
    private var apiInterface: APIInterface? = null

    /** @return the [APIInterface] object */
    fun getAPIInterface(): APIInterface {
        if (apiInterface == null) {
            val retrofit = Retrofit.Builder()
                    .baseUrl(App.context.getString(R.string.api_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(OkHttpClientHelper().initOkHttpClient())
                    .build()

            apiInterface = retrofit.create(APIInterface::class.java)
        }

        return apiInterface!!
    }

    /** The interface for retrofit's API calls */
    interface APIInterface {

        @GET("users?delay=5")
        fun fetchSamples()
                : Call<CoreResp<MutableList<Sample>>>
        //@Body body: Sample

        @GET("samples/{id}")
        fun fetchSample(
                @Path("id") id: String)
                : Call<CoreResp<Sample>>

    }
}