package com.maleskuliah.app.helper

import java.text.ParseException

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Alvin Rusli on 10/7/2016.
 *
 * A helper class for date/time format.
 */
object DateHelper {

    /**
     * Convert time and date to millis.
     * @param dateTime The string to parse
     * @return time in millis
     */
    @Throws(ParseException::class)
    fun convertToMillis(dateTime: String, dateFormat: String): Long {
        val formatter = SimpleDateFormat(dateFormat, Locale.US)
        return formatter.parse(dateTime).time
    }

    /**
     * Obtain a [Calendar] object from a String.
     * @return the [Calendar] object
     */
    fun getCalendarFromMillis(millis: Long): Calendar {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = millis
        return calendar
    }

    /**
     * Obtain a [Calendar] object from a String.
     * @return the [Calendar] object
     */
    fun getCalendarFromDate(dateTime: String, dateFormat: String): Calendar? {
        try {
            val formatter = SimpleDateFormat(dateFormat, Locale.US)
            val date = formatter.parse(dateTime)
            val calendar = Calendar.getInstance()
            calendar.time = date
            return calendar
        } catch (e: ParseException) {
            Common.printStackTrace(e)
            return null
        }
    }


    fun parseDate(time: String?, inputPattern: String, outputPattern: String): String? {
        if (time.isNullOrEmpty()) return ""

        val localeId = Locale("in", "ID")
        val inputFormat = SimpleDateFormat(inputPattern, localeId)
        val outputFormat = SimpleDateFormat(outputPattern, localeId)

        var date: Date? = null
        var str: String? = null

        try {
            date = inputFormat.parse(time)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return str
    }

    fun toSimpleDate(time: String?): String? {
        return parseDate(time, "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy")
    }

    fun toTimestamp(time: String?): String? {
        return parseDate(time, "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm")
    }
}
