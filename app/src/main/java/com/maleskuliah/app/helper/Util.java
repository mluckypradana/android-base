package com.maleskuliah.app.helper;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Build;
import android.support.annotation.RequiresApi;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by mobile75
 */

public class Util {
    public static String getResponse(Response<ResponseBody> response) throws IOException {
        String json = null;

        if(response != null) {
            try {
                if (response.code() == 200)
                    json = new String(response.body().bytes());
                else
                    json = new String(response.errorBody().bytes());
            } catch (Exception e) {
                if (response.code() == 200)
                    json = response.body().string();
                else
                    json = response.body().string();
                e.printStackTrace();
            }
        }

        return json;
    }

    public static boolean saveBitmapToFile(String path, Bitmap bitmap) {
        File file 	= new File(path);

        if(file.exists()) file.delete();


        boolean res = false;

        if (!file.exists()) {
            try {
                FileOutputStream fos = new FileOutputStream(file);

                res = bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);

                fos.close();
            } catch (Exception e) { }
        }

        return res;
    }

    @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
    public static Bitmap decodeSampledBitmapFromUri(String path, int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds = true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return handingRotatioImage(path, BitmapFactory.decodeFile(path, options));
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight){
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;

        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float)height / (float)reqHeight);
            }else {
                inSampleSize = Math.round((float)width / (float)reqWidth);
            }
        }

        return inSampleSize;
    }

    @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
    public static Bitmap handingRotatioImage(String photoPath, Bitmap bitmap) {
        Bitmap myBitmap = bitmap;
        try {
            ExifInterface ei = new ExifInterface(photoPath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            switch(orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    myBitmap = rotateImage(bitmap, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    myBitmap = rotateImage(bitmap, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    myBitmap = rotateImage(bitmap, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:

                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return myBitmap;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public static  String getStringValue(Context context, int stringId) {
        return context.getResources().getString(stringId);
    }
    public static String getPhotoName(String imagePath) {
        String fileName     = "";

        try {
            String[] fileSplit  = imagePath.split("\\/");

            fileName  = fileSplit[fileSplit.length - 1];
        } catch (Exception e) {
            e.printStackTrace();
        }

        return  fileName;
    }

    public static float getFileSize(String path) {
        float size = 0;

        try{
            File file       = new File(path);
            float filesize  = file.length();
            filesize        = filesize/1024;
            String value    = "";

            size = filesize;

        }catch(Exception e){
            System.out.println("File not found : " + e.getMessage() + e);
        }

        return size;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    public static int getFileSize(Bitmap data) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return data.getRowBytes() * data.getHeight();
        } else {
            return data.getByteCount();
        }
    }

    public static Date convertStringtoDate(String value, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(value);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return convertedDate;
    }

    public static String convertStringToDateString(String value, String format, String toFormat) {
        SimpleDateFormat form = new SimpleDateFormat(format);
        Date date = null;
        try {
            date = form.parse(value);
            SimpleDateFormat postFormater = new SimpleDateFormat(toFormat);
            String newDateStr = postFormater.format(date);//.toUpperCase();
            System.out.println("Date  : " + newDateStr);

            return newDateStr;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static String convertDateToString(Date dateValue, String formatTo) {
        String mDate = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(formatTo);
            mDate = sdf.format(dateValue);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mDate;
    }


    public static String getTimeAgo(String srcDate) {
        if(srcDate == null || srcDate.isEmpty())
            return "";

        SimpleDateFormat desiredFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date past = desiredFormat.parse(srcDate);
            Date now = new Date();
            long seconds= TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime());
            long minutes=TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime());
            long hours=TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime());
            long days=TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime());

            if(seconds<60) {
                return "baru saja";
            } else if(minutes<60) {
                return minutes + " menit lalu";
            } else if(hours<24) {
                return hours + " jam lalu";
            } else if(days<7) {
                return days + " hari lalu";
            } else {
                SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
                return df.format(past);
            }
//            dateInMillis = date.getTime();
//            return DateUtils.getRelativeTimeSpanString(
//                    dateInMillis, System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }

    // convert UTF-8 to internal Java String format
    public static String convertUTF8ToString(String s) {
        String out = null;
        try {
            out = new String(s.getBytes("ISO-8859-1"), "UTF-8");
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return out;
    }

    // convert internal Java String format to UTF-8
    public static String convertStringToUTF8(String s) {
        String out = null;
        try {
            out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return out;
    }

    public static String replaceChar(String data, String olChar, String newChar) {
        try {
            data = data.replace(olChar, newChar);
        } catch (Exception e) {}

        return data;
    }
}
