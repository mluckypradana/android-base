package com.maleskuliah.app.listener

/**
 *
 * Created by MuhammadLucky on 07/03/2018.
 */

interface OnClickListener {
    fun onClicked()
}
