package com.maleskuliah.app.listener

/**
 * Created by MuhammadLucky on 24/08/2016.
 */
interface ScrollListener {
    fun onScrolledTop()

    fun onScrolledBottom()
}
