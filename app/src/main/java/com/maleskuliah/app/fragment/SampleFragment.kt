package com.maleskuliah.app.fragment

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.kopnus.kdigi.presenter.callback.FetchDataCallback
import com.maleskuliah.app.R
import com.maleskuliah.app.adapter.SampleAdapter
import com.maleskuliah.app.application.App
import com.maleskuliah.app.fragment.core.CoreFragment
import com.maleskuliah.app.listener.OnItemClickListener
import com.maleskuliah.app.model.local.Sample
import com.maleskuliah.app.viewmodel.SampleVm
import kotlinx.android.synthetic.main.frag_list.*
import kotlinx.android.synthetic.main.layout_no_data.*


/**
 * Fragment for content in
 * Created by MuhammadLucky on 1/25/2017.
 */
typealias CORE = Sample

class ListFragment : CoreFragment() {
    override val viewRes: Int? = R.layout.frag_list
    var list: MutableList<CORE> = arrayListOf()
    private lateinit var adapter: SampleAdapter
    private lateinit var vm: SampleVm

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initData()
    }

    private fun initView() {
        adapter = SampleAdapter(list, object : OnItemClickListener {
            override fun onItemClicked(position: Int) {
            }
        })
        rv_main.layoutManager = LinearLayoutManager(App.context, LinearLayoutManager.VERTICAL, false)
        rv_main.adapter = adapter
        adapter.addSeparator(rv_main, R.dimen.space)
    }


    private fun initData() {
        vm = SampleVm(this)

        showLoading(context!!)
        vm.fetchSamples(object : FetchDataCallback {
            override fun onFailure(message: String?) {
                tv_no_data.text = message
                hideLoading()
            }

            override fun onSuccess(response: Any?) {
                refreshData(response as MutableList<CORE>)
                hideLoading()
            }
        })
    }

    private fun refreshData(list: MutableList<CORE>) {
        this.list.clear()
        this.list.addAll(list)
        adapter.notifyDataSetChanged()
    }

    override fun showLoading(context: Context) {
//        super.showLoading(context)
        vf_main.displayedChild = if (list.isEmpty()) 1 else 0
    }

    override fun hideLoading() {
//        super.hideLoading()
        vf_main.displayedChild = if (list.isEmpty()) 2 else 0
    }
}
