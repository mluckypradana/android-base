package com.maleskuliah.app.model.local

import com.google.gson.annotations.SerializedName
import com.maleskuliah.app.model.core.CoreModel

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
open class News : CoreModel() {
    @SerializedName("writer_id")
    var writerId: String? = null
    @SerializedName("category_id")
    var categoryId: String? = null
    @SerializedName("title")
    var title: String? = null
    @SerializedName("content")
    var content: String? = null
}