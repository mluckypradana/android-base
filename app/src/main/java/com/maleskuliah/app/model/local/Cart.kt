package com.maleskuliah.app.model.local

import com.google.gson.annotations.SerializedName
import com.maleskuliah.app.model.core.CoreModel

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
open class Cart : CoreModel() {
    @SerializedName("seminar_id")
    var seminarId: String? = null
    @SerializedName("buyer_id")
    var buyerId: String? = null
    @SerializedName("price")
    var price: String? = null
    @SerializedName("email")
    var email: String? = null
    @SerializedName("fullname")
    var fullname: String? = null

}