package com.maleskuliah.app.model.response.core

import com.maleskuliah.app.model.local.Sample

/**
 * Handle error response
 * Created by MuhammadLucky on 14/05/2018.
 */
class ErrorResp : CoreResp<Sample>()