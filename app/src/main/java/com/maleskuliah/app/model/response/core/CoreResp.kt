package com.maleskuliah.app.model.response.core

import com.google.gson.annotations.SerializedName

/**
 * Response with generic data
 * Created by MuhammadLucky on 14/05/2018.
 */
open class CoreResp<T>{
    @SerializedName("message")
    var message: String? = null
    @SerializedName("data")
    var data: T? = null
}