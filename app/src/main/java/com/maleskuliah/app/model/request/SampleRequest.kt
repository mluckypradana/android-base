package com.maleskuliah.app.model.request

import com.google.gson.annotations.SerializedName
import com.maleskuliah.app.model.request.core.CoreReqs

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
class SampleRequest : CoreReqs() {
    @SerializedName("id")
    var id: String? = null
}
