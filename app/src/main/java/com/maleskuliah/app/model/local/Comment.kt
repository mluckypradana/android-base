package com.maleskuliah.app.model.local

import com.google.gson.annotations.SerializedName
import com.maleskuliah.app.model.core.CoreModel

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
open class Comment : CoreModel() {
    @SerializedName("new_id")
    var news_id: String? = null
    @SerializedName("comment")
    var comment: String? = null
    @SerializedName("comment_tail")
    var commentTail: String? = null
}