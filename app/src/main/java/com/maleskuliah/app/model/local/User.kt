package com.maleskuliah.app.model.local

import com.google.gson.annotations.SerializedName
import com.maleskuliah.app.model.core.CoreModel

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
open class User : CoreModel() {
    @SerializedName("username")
    var username: String? = null
    @SerializedName("level_id")
    var levelId: String? = null
    @SerializedName("fullname")
    var fullname: String? = null
    @SerializedName("email")
    var email: String? = null
    @SerializedName("instantion")
    var instantion: String? = null
    @SerializedName("phone")
    var phone: String? = null

}