package com.maleskuliah.app.model.local

import com.google.gson.annotations.SerializedName
import com.maleskuliah.app.model.core.CoreModel

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
open class Faq : CoreModel() {
    @SerializedName("title")
    var title: String? = null
    @SerializedName("description")
    var description: String? = null
}