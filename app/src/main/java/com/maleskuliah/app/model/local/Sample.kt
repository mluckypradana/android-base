package com.maleskuliah.app.model.local

import com.google.gson.annotations.SerializedName
import com.maleskuliah.app.model.core.CoreModel

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
open class Sample : CoreModel() {
    @SerializedName(value = "title", alternate = ["first_name", "name"])
    var title: String? = null

}