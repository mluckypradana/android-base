package com.maleskuliah.app.model.request.core

import com.google.gson.annotations.SerializedName
import com.maleskuliah.app.BuildConfig

/**
 * Created by MuhammadLucky on 14/05/2018.
 */
open class CoreReqs{
    @SerializedName("version")
    var version: String? = BuildConfig.VERSION_NAME
}
