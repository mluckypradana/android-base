package com.maleskuliah.app.activity

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.WindowManager
import com.maleskuliah.app.BuildConfig
import com.maleskuliah.app.R
import com.maleskuliah.app.activity.core.CoreActivity
import com.maleskuliah.app.config.Config
import com.maleskuliah.app.viewmodel.PrefVm
import kotlinx.android.synthetic.main.act_splash.*

class SplashAct : CoreActivity() {
    override val viewRes: Int? = R.layout.act_splash
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableFullscreen()
        initSplashScreen()

        tv_version.text = getString(R.string.label_version_val, BuildConfig.VERSION_NAME)
    }

    private fun enableFullscreen() {
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                View.SYSTEM_UI_FLAG_FULLSCREEN or
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        val attrib = window.attributes
//        attrib.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES
    }


    private fun initSplashScreen() {
        val handler = Handler()
        handler.postDelayed({ showMainPage() }, Config.SPLASH_DELAY)
    }

    private fun showMainPage() {
        //Check pref version
        PrefVm.checkVersion()

        //If app uses account setting
//        AuthPresenter.setLoginFromSetting(false)

        //Check user
//        var isLoggedIn = AuthPresenter.isLoggedIn()
//        if (isLoggedIn) {
//            var user = UserPresenter.getUser()
//            if (user!!.isGuest()) {
//                AuthPresenter().logout(this)
//                isLoggedIn = false
//            }
//        }

//        if (PrefPresenter.shouldShowIntro()) {
//            PrefPresenter.setShowedIntro()
//            IntroActivity.launchIntent(this@SplashScreenActivity)
//            return
//        }

        //Redirect
//        if (isLoggedIn)
//            MainActivity.launchIntent(this@SplashScreenActivity)
//        else
//            LandingActivity.launchIntent(this@SplashScreenActivity)

        launchActivity(MainAct::class.java)
        finish()
    }
}
