package com.maleskuliah.app.activity.core

import android.content.Context
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import com.maleskuliah.app.R
import com.maleskuliah.app.helper.Common
import com.maleskuliah.app.helper.IntentHelper


/**
 * Created by MuhammadLucky on 06/07/2017.
 */
abstract class CoreActivity : AppCompatActivity() {
    internal var toggle: ActionBarDrawerToggle? = null
    var isFirstFragment = true

    /**
     * The layout res for the current activity.
     * @return the layout resource ID, return a null to make the activity not set a content view.
     */
    abstract val viewRes: Int?

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (viewRes != null) setContentView(viewRes!!)
    }

    /** Show wait progress dialog with general message */
    open fun showLoading(context: Context) {
        Common.showProgressDialog(context, R.string.message_wait, null)
    }

    /** Hide wait progress dialog with */
    open fun hideLoading() {
        Common.dismissProgressDialog()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    open fun initToolbar(toolbar: Toolbar) {
        initToolbar(toolbar, false)
    }

    /** Set support actionbar, add back button */
    open fun initToolbar(toolbar: Toolbar, showBackButton: Boolean) {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toggle?.syncState()
    }

    private var lastFragment: Fragment? = null

    open fun showFragment(fragment: Fragment?) {
        if (fragment == null) return

        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.ll_container, fragment)
        if (isFirstFragment)
            isFirstFragment = false
        else
            transaction.addToBackStack(null)
        lastFragment = fragment
        transaction.commit()
    }

    internal fun launchActivity(java: Class<*>) {
        IntentHelper.launch(this, java)
    }
}
