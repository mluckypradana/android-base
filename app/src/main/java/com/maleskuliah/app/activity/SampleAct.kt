package com.maleskuliah.app.activity

import com.maleskuliah.app.R
import com.maleskuliah.app.activity.core.CoreActivity

class SampleAct : CoreActivity() {
    override val viewRes: Int? = R.layout.act_sample
}
