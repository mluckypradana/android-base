package com.maleskuliah.app.activity

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.view.ViewPager
import com.maleskuliah.app.R
import com.maleskuliah.app.activity.core.CoreActivity
import com.maleskuliah.app.adapter.ViewPagerAdapter
import com.maleskuliah.app.fragment.ListFragment
import kotlinx.android.synthetic.main.act_main.*

class MainAct : CoreActivity() {
    override val viewRes: Int? = R.layout.act_main
    private lateinit var adapter: ViewPagerAdapter
    val homeFragment = ListFragment()
    val listFragment = ListFragment()
    val notificationFragment = ListFragment()

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                vp_main.setCurrentItem(0, false)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                vp_main.setCurrentItem(1, false)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        nv_main.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        initView()
        initData()
    }

    private fun initData() {
    }

    private fun initView() {
        adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(homeFragment, getString(R.string.title_home))
        adapter.addFragment(listFragment, getString(R.string.title_dashboard))
        adapter.addFragment(notificationFragment, getString(R.string.title_notifications))
        vp_main.adapter = adapter
        vp_main.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                val id = when (position) {
                    0 -> R.id.navigation_home
                    1 -> R.id.navigation_dashboard
                    2 -> R.id.navigation_notifications
                    else -> R.id.navigation_home
                }
                nv_main.selectedItemId = id
            }
        })
    }
}
