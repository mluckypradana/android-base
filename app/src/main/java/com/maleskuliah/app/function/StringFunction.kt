package com.maleskuliah.app.function

fun String.checkHttpStartWith() : String {
    if (this.startsWith("http")) return this
    else return "http://" + this
}