package com.maleskuliah.app.config

/**
 * Created by MuhammadLucky on 05/02/2018.
 */

object Prefs {
    const val KEY = "KEY"
    const val PREF_VERSION = "PREF_VERSION"
    const val TEMP_DATA = "TEMP_DATA"
}
