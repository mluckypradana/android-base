package com.maleskuliah.app.config

/**
 * Created by MuhammadLucky on 05/02/2018.
 */

object Extras {
    const val KEY = "KEY"
    const val PREF_VERSION = "PREF_VERSION"
}
